package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(6);
        System.out.println(crossSum(153));
        System.out.println(" ");

        /*int sum = crossSum(1);
        System.out.println(sum); // 1

        int sum = crossSum(12);
        System.out.println(sum); // 3

        int sum = crossSum(123);
        System.out.println(sum); // 6

        int sum = crossSum(1234);
        System.out.println(sum); // 10

        int sum = crossSum(4321);
        System.out.println(sum); // 10*/

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 1; i < (n+1); i++) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j < (n*i)+1; j++) {
                if (j % i == 0) {
                    System.out.print(j + " ");
                }
            }
            System.out.println();
        }
    }


    public static int crossSum(int num) {
        int crosssum = 0;
        while (num > 0) {
            crosssum += (num % 10);
            num = (num / 10);
        }
        return crosssum;
    }

}
