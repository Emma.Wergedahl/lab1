package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        //Oppgave 1
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));
        
        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }
        // [21, 22, 23]
        // [31, 32, 33]
        
        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));
        
        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }
        // [11, 12, 13]
        // [31, 32, 33]
        
        
        //Oppgave 2
        ArrayList<ArrayList<Integer>> grid4 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grid4);
        System.out.println(equalSums1); // false


        ArrayList<ArrayList<Integer>> grid5 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid5);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true



    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (allRowsAreEqualSum(grid) && allColsAreEqualSum(grid)) {
            return true;
        }
        else {
            return false;
        }

    }

    public static boolean allRowsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> newList = new ArrayList();
        int x = 0;
        int y = 0;  //Times two numbers in newList are equal

        while (x < grid.get(0).size()) {
            int sum = 0;
            for (int i=0; i<grid.get(0).size(); i++) {
                sum += grid.get(x).get(i);
            }
            newList.add(sum);
            x += 1;
        }
        
        for (int i=0; i<(newList.size()-1); i++) {
            if (newList.get(i).equals(newList.get(i+1))) {
                y += 1;
            } 
        }
        
        if (y == (newList.size()-1)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean allColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> newList = new ArrayList<>();
        int x = 0;
        int y = 0;

        while (x < grid.get(0).size()) {
            int sum = 0;
            for (int i=0; i<grid.get(0).size(); i++) {
                sum += grid.get(i).get(x);
            }
            newList.add(sum);
            x += 1;
        }
        System.out.println(newList);
        for (int i=0; i<(newList.size()-1); i++) {
            if (newList.get(i).equals(newList.get(i+1))) {
                y += 1;
            } 
        }
        
        if (y == (newList.size()-1)) {
            return true;
        }
        else {
            return false;
        }    
    }
}
