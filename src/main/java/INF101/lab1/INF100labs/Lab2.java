package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("apple", "carrot", "ananas");
        findLongestWords("Game", "Action", "Champion");
        findLongestWords("Four", "Five", "Six");
        
        boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); // false

        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); // true

        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); // false

        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4); // true

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int length1, length2, length3;
        length1 = word1.length();
        length2 = word2.length();
        length3 = word3.length();
        

        if (length1 > length2 && length1 > length3) {
            System.out.println(word1);

        } else if (length2 > length1 && length2 > length3) {
            System.out.println(word2);

        } else if (length3 > length1 && length3 > length2) {
            System.out.println(word3);

        } else if (length1 == length2 && length1 == length3) {
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);

        } else if (length1 == length2) {
            System.out.println(word1);
            System.out.println(word2);
        }
         else if (length2 == length3) {
            System.out.println(word2);
            System.out.println(word3);
         }
    }


    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 != 0) {
                return true;
            }
            else if (year % 100 == 0 && year % 400 == 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else return false;
    }



    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 == 0 && num >0) {
            System.out.println(true);
            return true;

        } else {
            return false;
        }
    }

}
